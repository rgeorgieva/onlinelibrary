$(document).ready(function () {

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

});

var priceSlider = document.getElementById('price-range-slider');
var priceOutput = document.getElementById('output');

priceSlider.addEventListener('input', function() {
    priceOutput.innerHTML = priceSlider.value;
})

var sizeSlider = document.getElementById('size-range-slider');
var sizeOutput = document.getElementById('size-output');

sizeSlider.addEventListener('input', function() {
    sizeOutput.innerHTML = sizeSlider.value;
})